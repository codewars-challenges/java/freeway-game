package be.hics.sandbox.freewaygame;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FreewayGameApplication {

    public static void main(String[] args) {
        SpringApplication.run(FreewayGameApplication.class, args);
        System.out.println(Dinglemouse.freewayGame(50.0, 130.0, new double[][]{{-1.0, 120.0}, {-1.5, 120.0}}));
        System.out.println(Dinglemouse.freewayGame(50.0, 110.0, new double[][]{{1.0, 120.0}, {1.5, 125.0}}));
        System.out.println(Dinglemouse.freewayGame(50.0, 120.0, new double[][]{{-1.0, 115.0}, {-1.5, 110.0},{1.0, 130.0}, {1.5, 130.0}}));
        System.out.println(Dinglemouse.freewayGame(30.0, 100.0, new double[][]{{-1.0, 110.0}, {-0.7, 102.0}, {-1.5, 108.0}}));
        System.out.println(Dinglemouse.freewayGame(30.0, 130.0, new double[][]{{1.0, 120.0}, {0.7, 125.0}, {1.5, 110.0}}));
    }
}
