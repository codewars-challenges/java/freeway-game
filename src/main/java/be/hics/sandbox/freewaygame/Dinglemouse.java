package be.hics.sandbox.freewaygame;

import java.util.Arrays;
import java.util.function.Function;
import java.util.function.Predicate;

public class Dinglemouse {

    public static int freewayGame(final double distKmToExit, final double mySpeedKph, final double[][] otherCars) {
        double myTimeToExit = timeToExit(distKmToExit, mySpeedKph);
        Function<double[], Integer> toCounter = otherCar -> calculate(myTimeToExit, timeToExit(distKmToExit, otherCar[1]), otherCar[0]);
        Predicate<double[]> overtakeable = otherCar -> (otherCar[0] < 0.0 && otherCar[1] < mySpeedKph) || (otherCar[0] > 0.0 && otherCar[1] > mySpeedKph);
        return Arrays.stream(otherCars).filter(overtakeable).map(toCounter).mapToInt(Integer::intValue).sum();
    }

    private static int calculate(final double myTimeToExit, final double otherTimeToExit, final double penalty) {
        if (penalty <= 0)
            return myTimeToExit < otherTimeToExit + penalty ? 1 : 0;
        else if (penalty > 0)
            return myTimeToExit > otherTimeToExit + penalty ? -1 : 0;
        else
            return myTimeToExit < otherTimeToExit + penalty ? 1 : -1;
    }

    private static double timeToExit(final double distKmToExit, final double speedKph) {
        return distKmToExit / speedKph * 60;
    }

}